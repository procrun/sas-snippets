/******************************************************************************
 File: daysUntil.sas

 Creates a macro called %daysUntil(event, date) that will find the number of days
 until an "event" taking place on a certain "date";
 
 Author: Andy M.  
 Twitter: ProcRun
 Web: http://www.procrun.com
********************************************************************************/
%macro daysUntil(event, date);
  data _null_;
    diff = datdif(today(), input("&date.", anydtdte10.), 'act/act');
    call symput('days_until', strip(diff));
  run;

  %put There are &days_until days until &event;
%mend;





