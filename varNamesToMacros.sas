/******************************************************************************
 File: varNamesToMacros.sas

 Code will automatically populate macro variables based on the variable names
 of a data set.  Useful if you need to perform the same operation over 
 multiple variables.   

 Modify the proc contents statment to get the list of all datasets in within
 a library.  (libbame._all_)

 Example data is the sashelp.yr1001 data set which has 505 variables.  
 A ttest will  be performed on each variable.  To limit this, change the 
 maxium varable &count. to a smaller value in each macro loop.    

 Author: Proc 
 Twitter: ProcRun
 Web: http://www.procrun.com
********************************************************************************/

*--- Get all the variable names from the data set ---;
proc contents data = sashelp.yr1001
				out = names (keep = name)
				noprint;
run;

*--- Put count and distinct names into macro variables ---;
proc sql;
 select
 	distinct name,
	count(distinct name)
 into
 	:name1 - :name9999, 
	:count
 from
	names
 where
 	substr(name, 1, 1) = 'S';
quit;

*--- List all of the variables in the log ---;
%macro loop();
 %do i = 1 %to &count.;
   %put &&name&i.;
 %end;
%mend;
%loop();

*--- Instead of listing to the log, perform some other proc ---;
%macro loop2();
 %do i = 1 %to &count.;
  proc ttest data = sashelp.yr1001;
   var &&name&i.;
  run;
 %end;
%mend;
%loop2();


 
